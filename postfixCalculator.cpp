// Meghan O'Hara mmo2bf 9/14/16 postfixCalculator.cpp

#include "postfixCalculator.h"
#include <iostream>

using namespace std;

postfixCalculator::postfixCalculator() {
}

void postfixCalculator::add() {
  int firstNum = stk.top();
  stk.pop(); 
  int secNum = stk.top();
  stk.pop(); 
  int returnNum = firstNum + secNum;
  stk.push(returnNum); 
}

void postfixCalculator::subtract() {
  int firstNum = stk.top();
  stk.pop(); 
  int secNum = stk.top();
  stk.pop(); 
  int returnNum = secNum - firstNum;
  stk.push(returnNum);
}

void postfixCalculator::divide() {
  int firstNum = stk.top();
  stk.pop(); 
  int secNum = stk.top();
  stk.pop(); 
  int returnNum = secNum / firstNum;
  stk.push(returnNum);
}

void postfixCalculator::multiply() {
  int firstNum = stk.top();
  stk.pop(); 
  int secNum = stk.top();
  stk.pop(); 
  int returnNum = firstNum * secNum;
  stk.push(returnNum);
}


void postfixCalculator::negate() {
  int tempValue = stk.top();
  stk.pop(); 
  int negTempValue = -tempValue;
  stk.push(negTempValue); 
}

void postfixCalculator::push(int e) {
  stk.push(e); 
}
void postfixCalculator::pop() {
  stk.pop(); 
}
int postfixCalculator::top() {
  return stk.top(); 
}
void postfixCalculator::empty() {
  stk.empty(); 
}



