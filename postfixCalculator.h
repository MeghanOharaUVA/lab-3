// Meghan O'Hara mmo2bf 9/14/16 postfixCalculator.h

#ifndef POSTFIXCALCULATOR_H
#define POSTFIXCALCULATOR_H

#include "List.h"
#include "ListItr.h"
#include "ListNode.h"
#include "stack.h"

#include <iostream>


using namespace std;

class postfixCalculator {
 public:
  postfixCalculator(); 
  void add();
  void subtract(); 
  void divide();
  void multiply(); 
  void negate();
  void push(int e);
  int top();
  void pop();
  void empty(); 

 private:
  Stack stk;
  friend class testPostfixCalc; 
  
} ; 

#endif
