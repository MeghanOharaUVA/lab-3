// Meghan O’Hara mmo2bf 09/05/2016 ListItr.cpp

#include "ListItr.h"
#include <iostream>
using namespace std;

// Constructor 
ListItr::ListItr() {
  current = NULL;
}

// One parameter constructor 
ListItr::ListItr(ListNode* theNode) {
  current = theNode;
}

// True if past end position in list, else false 
bool ListItr::isPastEnd() const{
  if ( current->next == NULL ) {
    return true;
  }

  else {
    return false;
  }
}

// True if past first position in list, else false
bool ListItr::isPastBeginning() const{
  if ( current->previous == NULL ) {
      return true;
   }
  
  else {
    return false;
  }
}

// Advances current to next position in list
// (unless already past end)
void ListItr::moveForward(){
  if ( isPastEnd() == false ) {
      current = current->next;
  }
}

// Moves current back to previous position in list
// (unless already past beginning)
void ListItr::moveBackward(){
  if ( isPastBeginning() == false )  {
     current = current->previous; 
  }
}

// Returns item in current position 
int ListItr::retrieve() const{
  return current->value; 
}
