// Meghan O'Hara mmo2bf 9/14/16 stack.cpp

#include "stack.h"
#include <iostream>

using namespace std;

 

Stack::Stack() {
}

void Stack::push(int e) {
  ListItr iter(list.first()); 
  list.insertBefore(e, iter);
}

int Stack::top() {
  if (!empty()) {
    ListItr iter(list.first()); 
    int retValue = iter.retrieve();
    return retValue;
  }
  else {
    exit(-1);
  }
}

void Stack::pop() {
  if (!empty()) {
    ListItr iter(list.first());
    int remValue = iter.retrieve(); 
    list.remove(remValue);
 }
  
  else {
    exit(-1);
  }
}

bool Stack::empty() {
  return list.isEmpty(); 
}
