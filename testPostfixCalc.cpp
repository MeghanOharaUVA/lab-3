//Meghan O'Hara mmo2bf 9/14/16 testPostfixCalc.cpp

#include <iostream>
#include "postfixCalculator.h"
#include "List.h"
#include "ListItr.h"
#include "ListNode.h"
#include "stack.h"

using namespace std; 

/* 

Testers: 

1 2 3 4 5 + + + +          = 15
20 10 - -3 10 - - 2 -      = 21
-1 -2 -5 3 * 2 -2 * * * *  = 120
-1512 -12 -2 / / -2 / 3 /  = 42
-1 ~ ~ ~                   = 1

 */ 

int main() {
  
    postfixCalculator z;

    while(cin.good()) { 
    string s; 
    cin >> s;
 
    if (s != "") {
   
    
    if (isdigit(s[0])) {
      int num = atoi(s.c_str()); 
      z.push(num);
      int top = z.top();
    }

    else if (s.size() > 1) {
      int num = atoi(s.c_str()); 
      z.push(num); 
    }

    else if (s == "+") {
      z.add();
    }

    else if (s == "-") {
      z.subtract();
    }

    else if (s == "*") {
      z.multiply(); 
    }

    else if (s == "/") {
      z.divide();
    }
  
    else if (s  == "~") {
      z.negate();
    }
    } else {
      break;
    }
}
  
   
  cout << z.top() << endl; 
 
}
