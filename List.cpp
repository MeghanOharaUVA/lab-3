// Meghan O’Hara mmo2bf 09/05/2016 List.cpp

#include <iostream>
#include "List.h"
using namespace std;

// constructor
List::List(){
  head = new ListNode();
  tail = new ListNode();
  head->next = tail;
  tail->previous = head;
  count = 0;
}

// copy constructor
List::List(const List& source){
  head=new ListNode;
  tail=new ListNode;
  head->next=tail;
  tail->previous=head;
  count=0;
  ListItr iter(source.head->next);
  while (!iter.isPastEnd()) {       // deep copy of the list
      insertAtTail(iter.retrieve());
      iter.moveForward();
  }
}

// Destructor
List::~List(){
  makeEmpty(); 
  delete head;
  delete tail; 
}

// Equals
List& List::operator=(const List& source){
  if (this == &source)
    return *this;
  else {
  makeEmpty();
  ListItr iter(source.head->next);
  while (!iter.isPastEnd()) {
      insertAtTail(iter.retrieve());
      iter.moveForward();
    }
  }
  return *this;
}

// empty = true; else = false
bool List::isEmpty() const{
  if (count == 0) return true;
  return false;  
}

//Removes all items except blank head and tail
void List::makeEmpty() {
  ListItr itr(head->next);
  while(!(itr.isPastEnd())) {
    ListNode *toKill = itr.current;
    itr.moveForward(); 
    delete toKill;
  }
  
  count = 0; 
}

//Returns an iterator that points to the ListNode in the first position
ListItr List::first() {
  if (!isEmpty()) {
    ListItr first(head->next);
    return first;
  }
  else {
    return ListItr(tail);
  }
}

//Returns an iterator that points to the ListNode in the last position
ListItr List::last(){
   if (!isEmpty()) {
     ListItr last(tail->previous);
     return last;
   }
   else {
     return ListItr(head);
   }
}

//Inserts x after current iterator position p
void List::insertAfter(int x, ListItr position) {
  ListNode *aft = position.current->next;
  ListNode *curr = position.current; 
  ListNode *newNode = new ListNode();
  newNode->value = x; // set new node val
  newNode->next = aft;// set new node next 
  aft->previous = newNode;
  newNode->previous = curr;
  curr->next = newNode; // set curr next 
  count++; 
}


//Inserts x before current iterator position p
void List::insertBefore(int x, ListItr position) {
  ListNode *bef = position.current->previous;
  ListNode *curr = position.current; 
  ListNode *newNode = new ListNode();
  newNode->value = x;
  bef->next = newNode;
  newNode->next = curr;
  curr->previous = newNode;
  newNode->previous = bef; 
  count++; 
}

//Insert x at tail of list
void List::insertAtTail(int x) {
  ListNode *bef = tail->previous;
  ListNode*newNode = new ListNode();
  newNode->value = x;
  bef->next = newNode;
  newNode->next = tail;
  tail->previous = newNode;
  newNode->previous = bef; 
  count++; 
  }

//Removes the first occurrence of x
void List::remove(int x) {
  ListNode *delNode = find(x).current;
  ListNode *bef = delNode->previous;
  ListNode *after = delNode->next;
  bef->next = after;
  after->previous = bef; 
  delete delNode; 
  count--; 
}

// Returns an iterator that points to
// the first occurrence of x, else return a iterator to the dummy tail node
ListItr List::find(int x){
  ListItr itr = first();
  while (itr.current != NULL) {
    if (itr.current->value == x) {
      return itr.current; 
    }
    else {
      itr.current = itr.current->next;
    }
    
  }
  return NULL; 
}

//Returns the number of elements in the list
int List::size() const{
  return count;
}

void printList(List& source, bool direction) {
  ListItr itr = source.first();
  
  if (direction == true) {
    while (!(itr.isPastEnd())) {
      cout << itr.retrieve();
      cout << " "; 
      itr.moveForward(); 
    }

  }

  else {
    itr = source.last(); 
    while(!(itr.isPastBeginning())) {
        cout << itr.retrieve();
	cout << " "; 
	itr.moveBackward(); 
    }
     
  }
  }


