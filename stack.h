// Meghan O'Hara mmo2bf 9/14/16 stack.h

#ifndef STACK_H
#define STACK_H

#include <iostream>
#include <string>
#include "List.h"
#include "ListItr.h"
#include "ListNode.h"

using namespace std; 

class Stack {

 public:
  Stack(); 
  void push(int e);
  int top();
  void pop();
  bool empty();

 private:
  List list;
  
}; 



#endif
